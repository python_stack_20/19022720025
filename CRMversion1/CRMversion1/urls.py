"""CRMversion1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from django.contrib import admin
from crm_app_01 import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # 首页
    url(r'^index/', views.index, name='index'),
    # 注册
    url(r'^register/', views.register, name='register'),
    # 登录
    url(r'^login/', views.login, name='login'),
    # 验证码
    url(r'^get_valid_img/', views.get_valid_img, name='get_valid_img'),
    # 注销
    url(r'^logout/', views.logout, name='logout'),
    # url(r'^shopping/', views.shopping, name='shopping'),
    # 展示所有客户信息
    url(r'^customers/list/', views.Customers.as_view(), name='customers'),

    # 分页
    url(r'^test/', views.test, name='test'),

    # 添加
    url(r'^add/', views.AddCustomer.as_view(), name='addcustomer'),
    # 编辑
    url(r'^editcustomer/(\d+)/', views.EditCustomer.as_view(), name='editcustomer'),
    # 删除
    url(r'^deletecustomer/(\d+)/', views.DeleteCustomer.as_view(), name='deletecustomer'),
    # 搜索
    # url(r'^customers/list/?condition', views.Search.as_view(), name='search'),

]
