from wsgiref.simple_server import make_server

def application(environ,start_response):
    start_response('200 ok',[('Content-Type','text/html'),('k1','v1')])
    print(environ)
    print(environ['PATH_INFO'])
    return [b'<h1>Hello, web!</h>']
httpd = make_server('127.0.0.1',8001,application)
print('Serving HTTP on part 8001...')
httpd.serve_forever()
