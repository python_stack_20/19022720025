import time
from wsgiref.simple_server import make_server


def app(environ, start_response):
    start_response('200 ok', [('Content-Type', 'text/html'), ('k1', 'v1')])
    print(environ)
    path = environ['PATH_INFO']

    for urlpattern in urlpatterns:
        print(path)
        if urlpattern[0] == path:
            ret = urlpattern[1]()
            return ret


def html():
    with open('01 index2.html', 'rb') as f:
        data = f.read()
    return [data]


def home():
    current_time = str(time.time())
    with open('home.html', 'r', encoding='utf-8')as f:
        data = f.read()
        data = data.replace('xxoo', current_time).encode('utf-8')
    return [data]


urlpatterns = [
    ('/', html),
    ('/home.html', home),
]
httpd = make_server('127.0.0.1', 8001, app)
httpd.serve_forever()
