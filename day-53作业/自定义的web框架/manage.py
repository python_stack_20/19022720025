from wsgiref.simple_server import make_server
from urls import urlpatterns


def application(environ,start_response):
    start_response('200 ok',[('Content-Type','text/html')])
    path = environ['PATH_INFO']
    for urlpattern in urlpatterns:
        if path == urlpattern[0]:
            data = urlpattern[1](environ)
            break
    else:
        data = b'404 not found'
    return [data]

httpd = make_server('127.0.0.1',8001,application)

print('Serving HTTP on port 8001...')
httpd.serve_forever()



