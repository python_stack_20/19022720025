from views import auth,login,order


urlpatterns = [
    ('/login',login),
    ('/auth',auth),
    ('/order',order)
]