import socket
import time
from threading import Thread


sk = socket.socket()  # 建立socket连接
sk.bind(('127.0.0.1', 9001))  # 绑定主机端口
sk.listen()  # 保持监听


def html(conn):  # 发送index网页
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    with open('01 index2.html', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()


def css(conn):  # 发送index的css文件
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n')
    with open('index.css', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()


def js(conn):  #
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    with open('index.js', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()


def ico(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n')
    with open('favicon.ico', 'rb') as f:
        data = f.read()
    conn.send(data)
    conn.close()


def jpg(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    with open('1.jpg', 'rb')as f:
        data = f.read()
    conn.send(data)
    conn.close()


def home(conn):
    conn.send(b'HTTP/1.1 200 ok \r\nk1:v1\r\n\r\n')
    current_time = str(time.time())
    with open('home.html', 'r',encoding='utf-8')as f:
        data = f.read()
        data = data.replace('xxoo', current_time)
    conn.send(data.encode('utf-8'))
    conn.close()


urlpatterns = [
    ('/', html),
    ('/index.css', css),
    ('/index.js', js),
    ('/favicon.ico', ico),
    ('/1.jpg', jpg),
    ('/home.html', home),
]


def communication(conn):
    client_msg = conn.recv(1024).decode('utf-8')
    path = client_msg.split('\r\n')[0].split()[1]

    for urlpattern in urlpatterns:
        print(path)
        if urlpattern[0] == path:
            t = Thread(target=urlpattern[1], args=(conn,))
            t.start()


while 1:
    conn, addr = sk.accept()
    t = Thread(target=communication, args=(conn,))
    t.start()
