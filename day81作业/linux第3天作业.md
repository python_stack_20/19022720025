1.灵活敲打linux命令

2.理解python虚拟环境配置

3.使用virtualenvwrapper工具管理虚拟环境，并且在虚拟环境下通过requirements.txt文件，保证本地开发环境和线上一致性
使用虚拟环境工具，分别启动django1和django2，并且截图

配置好阿里云yum源
生成yum缓存
下载nginx，并且启动nginx服务，使用浏览器访问，nginx页面



4.yum源的工作目录是？

`/etc/yum.repos.d/`

2.下载lrzsz工具，方便linux和windows互相传文件

`yum install lezsz` 

3.如何手动启动网卡？

`ifup  网卡名`

4.linux的超级用户是什么？如何查看用户身份信息？

```
root
whoami
```

5.简单描述linux的用户管理

```
系统默认一个超级用户root,可以使用   `useradd	用户名`	创建普通用户
```

6.如何创建普通用户，并且修改用户密码，然后使用普通用户登录

`useradd	用户名` 	`passwd	用户名`	

7.在linux下如何切换用户

`su	-	用户名`

8.普通用户如何使用root身份执行命令?请详细说明配置步骤

```
1.用root打开/etc/sudoers文件
2.找到如下行进行配置	(visudo命令,也是打开/etc/sudoers文件,并且提供语法检测功能)
        root    ALL=(ALL)       ALL
        普通用户名   ALL=(ALL)       ALL
3.普通用户下在输入:
sudo	命令
```

9.简述linux文件的权限有哪些？

```
r    read可读，可以用cat等命令查看 
w    write写入，可以编辑或者删除这个文件
x    executable   可以执行	
```

10.linux文件权限的755，700是什么意思？

```
755:属主可读可写可执行,属组可读可执行,其他用户可读可执行

700:属主可读可写可执行,属组无权限,其他用户无权限
```

11.如何修改test.py文件权限为700

`chmod	700		test.py`

12.如何修改test.py属组是oldboy?

`chgrp	oldboy		test.py`

13.已知test.py文件权限是rwxr--r--，如何修改权限为rw-rw-rw

`chmod	666		test.py`

14.linux如何建立软连接？

`ln	-s	/tmp/test.txt	my_test`

15.linux的PS1变量是什么？如何修改

```
系统命令提示符	PS1=新内容
```

16.centos7用什么命令管理服务

`systemctl` 

17.linux下dns解析的命令是什么？

​	`nslookup	域名`

18.将/tmp/下所有内容压缩成All_log.tar.gz并且放到/home/下

`cd	/home/`

`tar	-zcvf	All_log.tar.gz	/tmp/*`

19.解压缩Python源码包Python-3.7.0b3.tgz

`tar	-zxvf	Python-3.7.0b3.tgz`

20.查看mysql端口状态

`netstat	-an	|grep	3306`

21.如何查看nginx的进程

 `ps	–ef	|	grep	nginx`

22.如何杀死nginx进程

`killall	nginx`

23.如何修改linux中文

`vim /etc/locale.conf`
`LANG=zh_CN.UTF-8`

24.如何统计/var/log大小

`du	sh	/var/log`

25.tree是什么作用?

```
树状图列出目录的内容
```

26.如何给linux添加一个dns服务器记录

`vim /etc/reslov.conf`

27.每月的,5,15,25天的晚上5点50重启nginx

`50	17	5,15,25	*	*	/opt/nginx/sbin/nginx	-s	reload`

28.每周3到周5的深夜11点，备份/var/log /vmtp/

`0	23	*	*	3-5	/user/bin/cp	/var/log /vmtp/`

29.每天早上6.30清空/tmp/内容

`30	6	*	*	*	/user/bin/rm	-rf	/tep/*`

30.每个星期三的下午6点到8点的第5，15分钟执行命令 command

`5,15	18-20	*	*	3	command`

31.编译安装软件有哪些步骤？

```
1.下载源代码，它是一个压缩包
2.解压缩，源代码包
3.进入源代码包
4.找到 configure脚本，进行执行，释放makefile的指令
5.通过linux的 make指令，开始编译
6.开始编译且安装软件， make install 
```

32.如何修改python3的环境变量，以及软连接

```
1.echo	$PATH
2.PATH的值的前面加上"/opt/python36/bin:"
3.输入vim	/etc/profile在文档最后加上PATH的值,就可以永久配置python3的环境变量
4.ln	-s	/opt/python36/bin/python3.6	/usr/local/bin/python3
```

33.请在linux上启动ob_crm，windows上进行访问页面，进行登录

34.一月一号的4点重启nginx

`0	4	1	1	*	/opt/nginx/sbin/nginx	-s	reload`

35.每月的4号与每周一到周三的11点重启nginx

`0	11	4	*	1-3	/opt/nginx/sbin/nginx	-s	reload`

36.每小时重启一次nginx

`0	*	*	*	*	/opt/nginx/sbin/nginx	-s	reload`

37.每天10:00、16:00重启nginx

`0	10,16	*	*	*	/opt/nginx/sbin/nginx	-s	reload`

38.如何查看系统发行版信息

`cat	/etc/redhat-release`

39.系统全局环境变量配置文件是？

`/etc/profile`

40.系统用户的环境变量配置文件是？

`~/.bash_profile`

41.如何查看内存大小信息？

`free	-m`

42.如何查看cpu核数?

`cat /proc/cpuinfo | grep "physical id"`  

43.如何停止centos7防火墙服务，并且禁止防火墙开机自启

```
systemctl stop firewalld  #临时关闭防火墙服务
systemctl disable firewalld #永久关闭开机自启
```

`



