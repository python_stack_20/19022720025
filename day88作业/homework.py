'''
STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

要求:
1.登录页面
2.学生概况页面 ID name 点击详情
3.学生详情页面 ID name age gender
'''
from flask import Flask, request, render_template, session, redirect

app = Flask(__name__)
app.secret_key = "wahaha"

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}





@app.route("/login", methods=['post', 'get'])
def login():
    if request.method == "GET":
        return render_template("login.html")


    return render_template("StudentProfiles.html", data=STUDENT_DICT)
    # else:
    #     return redirect("/login")


@app.route("/details")
def details():
    id = int(request.args.get('id'))
    detail_dict = STUDENT_DICT.get(id)
    return render_template("Studentdetails.html", id=id, detail=detail_dict)


if __name__ == '__main__':
    app.run()
