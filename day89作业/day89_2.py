import re

from flask import Flask, request, render_template, session, redirect

from bp.details import details
from bp.profiles import profiles

app = Flask(__name__,static_folder="statics")
app.secret_key = "wahaha"

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

white_list = ["http://127.0.0.1:5000/login","http://127.0.0.1:5000/statics/*"]


@app.before_request
def br():
    if request.url not in white_list:
    # if not re.match("http://127.0.0.1:5000/login", request.url):
        print("br2")
        if not session.get("wahaha"):
            print("br3")
            return redirect("/login")
        session['count'] += 1
    print("br")
    return None


@app.route("/login", methods=['post', 'get'])
def login():
    if request.method == "GET":
        return render_template("login.html")

    uname = request.form.get("username")
    pwd = request.form.get("password")
    if uname == "jubing" and pwd == "123":
        session['wahaha'] = "jubing123"
        session["count"] = 0
        return redirect("/sprofiles")
    else:
        return redirect("/login")


@app.errorhandler(404)
def errer_page(a):
    print("404000")
    return render_template("404.html")

app.register_blueprint(profiles)

app.register_blueprint(details)

if __name__ == '__main__':
    app.run()

