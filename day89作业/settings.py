import hashlib
class Degbugsetting(object):
    DEBUG = True
    SCRET_KEY = hashlib.md5(b'debugsetting').hexdigest()
    SESSION_COOKIE_NAME = "I am debug session"


class Testingsetting(object):
    TESTING = True
    SCRET_KEY = hashlib.md5(b'testingsetting').hexdigest()
    SESSION_COOKIE_NAME = "I am Not session"

