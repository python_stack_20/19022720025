from flask import Blueprint, render_template, request


STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

details = Blueprint("details",__name__)


@details.route("/sdetails")
def sdetails():
    id = int(request.args.get('id'))
    detail_dict = STUDENT_DICT.get(id)
    return render_template("Studentdetails.html", id=id, detail=detail_dict)

