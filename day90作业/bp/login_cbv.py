from flask import views, render_template, request, redirect, Blueprint, session

# from flask_session import Session

login = Blueprint("login", __name__)

class Login(views.MethodView):
    def get(self):
        return render_template("login.html")

    def post(self):
        uname = request.form.get("username")
        pwd = request.form.get("password")
        if uname == "jubing" and pwd == "123":
            session['wahaha'] = "jubing123"
            session["count"] = 0
            return redirect("/sprofiles")
        else:
            return redirect("/login")