from flask import Blueprint, render_template

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

profiles = Blueprint("profiles", __name__)


@profiles.route("/sprofiles/")
def sprofiles():
    return render_template("StudentProfiles.html", data=STUDENT_DICT)
