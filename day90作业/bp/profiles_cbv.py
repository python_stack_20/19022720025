from flask import Blueprint, render_template, views

STUDENT_DICT = {
    1: {'name': 'Old', 'age': 38, 'gender': '中'},
    2: {'name': 'Boy', 'age': 73, 'gender': '男'},
    3: {'name': 'EDU', 'age': 84, 'gender': '女'},
}

profiles = Blueprint("profiles", __name__)


class Sprofiles(views.MethodView):
    def get(self):
        return render_template("StudentProfiles.html", data=STUDENT_DICT)
