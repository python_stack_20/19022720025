from flask import Flask, request, render_template, session, redirect, views
from bp.details_cbv import details, Sdetails
from bp.login_cbv import Login, login
from bp.profiles_cbv import Sprofiles, profiles
from flask_session import Session
from redis import Redis


app = Flask(__name__, static_folder="statics")

app.config['SECRET_KEY'] = 'wahaha'
# app.secret_key = "wahaha"
app.config['SESSION_TYPE'] = 'redis'
app.config['SESSION_REDIS'] = Redis(host='192.168.16.73',port=6380,db=1,password='111111')

Session(app)

white_list = ["http://127.0.0.1:5000/login", "http://127.0.0.1:5000/statics/*"]


@app.before_request
def br():
    if request.url not in white_list:
        # if not re.match("http://127.0.0.1:5000/login", request.url):
        if not session.get("wahaha"):
            return redirect("/login")
        session['count'] += 1
    return None


@app.errorhandler(404)
def errer_page(a):
    print("404000")
    return render_template("404.html")


app.register_blueprint(login)
app.register_blueprint(profiles)
app.register_blueprint(details)
app.add_url_rule("/login", view_func=Login.as_view(name="login"))
app.add_url_rule("/sdetails", view_func=Sdetails.as_view(name="sdetails"))
app.add_url_rule("/sprofiles", view_func=Sprofiles.as_view(name="sprofiles"))

if __name__ == '__main__':
    app.run()
