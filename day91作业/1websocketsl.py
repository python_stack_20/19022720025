import json
from flask import Flask, render_template, request
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)
user_socket_dict = {}

app.route("/my_socket/<username>")


def my_socket(username):
    user_socket = request.environ.get('wsgi.websocket')
    if user_socket:
        user_socket_dict[username] = user_socket
    while 1:
        msg = user_socket.receive()
        msg_dict = json.loads(msg)
        to_user_nick = msg_dict.get("to_user")
        to_user_socket = user_socket_dict.get(to_user_nick)
        to_user_socket.send(msg)


@app.route("/sl")
def sl():
    return render_template("sl.html")


if __name__ == '__main__':
    http_serv = WSGIServer(("0.0.0.0", 9527), app, handler_class=WebSocketHandler)
    http_serv.serve_forever()
