from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler
from geventwebsocket.server import WSGIServer
from geventwebsocket.websocket import WebSocket

app = Flask(__name__)
user_socket_dict = {}


@app.route("/my_socket/<username>")
def my_socket(username):
    user_socket = request.environ.get("wsgi.websocket")
    if user_socket:
        user_socket_dict[username] = user_socket
    while 1:
        msg = user_socket.receive()
        for usocket in user_socket_dict.values():
            try:
                usocket.send(msg)
            except:
                continue


if __name__ == '__main__':
    http_serv = WSGIServer(("0.0.0.0", 9527), app, handler_class=WebSocketHandler)
    http_serv.serve_forever()
