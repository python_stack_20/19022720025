from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler  # 提供ws协议处理
from geventwebsocket.server import WSGIServer  # 承载服务
from geventwebsocket.websocket import WebSocket  # 语法提示

app = Flask(__name__)
user_socket_list = []  # 连接列表


@app.route("/my_socket")
def my_socket():

    print(request.environ)
    user_socket = request.environ.get("wsgi.websocket")  # type:WebSocket
    # 获取当前客户端与服务器的socket连接
    if user_socket:
        user_socket_list.append(user_socket)  # 如果有连接过来就添加到连接列表
        print(len(user_socket_list), user_socket_list)
        while 1:
            msg = user_socket.receive()  # 服务器夯住等待接收
            print(msg)

            for usocket in user_socket_list:  # 给列表里的每个连接转发消息
                try:
                    usocket.send(msg)
                except:
                    continue


@app.route("/gc")
def gc():
    return render_template("gc.html")


if __name__ == '__main__':
    http_serv = WSGIServer(("192.168.16.6", 9527), app, handler_class=WebSocketHandler)
    http_serv.serve_forever()
