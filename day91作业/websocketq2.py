import json
from flask import Flask, request, render_template
from geventwebsocket.handler import WebSocketHandler  # 提供WS协议服务
from geventwebsocket.server import WSGIServer  # 承载服务
from geventwebsocket.websocket import WebSocket  # 语法提示

app = Flask(__name__)
user_socket_dict = {}  # 连接字典


@app.route("/my_socket/<username>")
def my_socket(username):
    user_socket = request.environ.get("wsgi.websocket")  # type:WebSocket
    # 获取当前客户端与服务器的socket连接
    if user_socket:
        user_socket_dict[username] = user_socket  # 如果连接来了就添加到连接字典
        print(len(user_socket_dict), user_socket_dict)
    while 1:
        msg = user_socket.receive()
        print(msg)
        for usocket in user_socket_dict.values():
            print(usocket)
            try:
                usocket.send(msg)
            except:
                continue


@app.route("/gc")
def gc():
    return render_template("gc2.html")


if __name__ == '__main__':
    http_serv = WSGIServer(("0.0.0.0", 9527), app, handler_class=WebSocketHandler)
    http_serv.serve_forever()
