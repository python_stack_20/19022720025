// 1.字符串所有方法写一遍

// var a=' 1 aABbCc';
// var b='bcd  2';
// var c=a+b;全部
// console.log(c);
// console.log(c.length);长度
// console.log(c.trim());去空格
// console.log(c.trimLeft());去左空格
// console.log(c.trimRight());去右空格
// console.log(c.charAt(1));返回索引对应的值
// console.log(c.concat('娃哈哈') );字符串的拼接
// console.log(c.indexOf('d', 3));在一串字符串中找到指定的元素对应的索引,从指定索引处开始找,没有返回-1
// console.log(b.substring(2, 6));类似于切片,但不能倒着切
// console.log(b.slice(-1, -5));切片
// console.log(a.toLowerCase());全部小写
// console.log(a.toUpperCase());全部大写

// 2.数组的所有方法写一遍
// var a=[123,'aBc','qq','dd','qqxing','娃哈哈'];
// var b=[1,22,333];
// console.log(a[3]);取数组中的元素
// console.log(a.length);查询数组长度
// console.log(a.push('wulala'),a);数组尾部追加元素
// console.log(a.pop(),a);数组尾部删除元素
// console.log(a.unshift('hello'),a);在数组开头插入元素
// console.log(a.shift(),a);数组开头删除元素
// console.log(a.slice(1, 4));切片
// console.log(a.reverse());反转
// console.log(a.join('哇嘿嘿'));以指定元素连接成字符串
// console.log(a.concat(b));数组的连接
// console.log(a.sort());排序

// 3.l1 = [11,22,33] 对数组进行for循环
// var l1 = [11,22,33];
// for(var i=0;i<l1.length;i++)
// {
//     console.log(l1[i]);
// }

// 4.d1 = {'name':'wuchao','age':18} 对字典进行循环
// var d1 = {'name':'wuchao','age':18};
// for (var key in d1)
// {
//     console.log(key+":"+d1[key]);
}
// 5.普通函数和匿名函数的定义方法
// 普通函数:
// function f1() {
//     console.log('hello world!');
// }
// 匿名函数:
// var sum = function (a,b) {
//     return a+b;
// }

// 6.js中json的序列化和反序列化的方法是什么
// 序列化:
// var obj = JSON.parse(str1);
// 反序列化:
// var str = JSON.stringify(obj1);