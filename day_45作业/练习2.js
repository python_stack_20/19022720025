
// 变量
    // 声明变量示例
        // var name = 'alex';
        // var age = 73;
        // var text1 =1,text2 = 'string';
        // console.log(name,age,text1,text2)
            // 弱类型变量
        // var test = 'hi';
        // alert(test)
        // test = 55;
        // alert(test)
            //声明过程中 var 非必须
        // stest = 'hello';
        // stest = stest+' hello'
        // alert(stest)


        // camel标记法:首字母小写,接下来的字母以大写字母开头
            // var myTestValue = 0,mySecondValue = 'hi';
            // alert(myTestValue)
            // alert(mySecondValue)
        // pascal标记法:首字母大写,接下来的字母以大写字母开头
            // var MyTestValue = 0,MySecondValue = 'hi';
            // alert(MyTestValue)
            // alert(MySecondValue)
        // 匈牙利类型标记法:在pascal标记法之前加上代表数据类型的小写字母,如i表示整数,s表示字符串
            // var iMyTestValue = 0,sMySecondValue = 'hi';
            // alert(iMyTestValue)
            // alert(sMySecondValue)


    // typeof 查看变量值的类型
        // var sTemp = 'test string';
        // alert(typeof sTemp)
        // typeof 返回值:
/*
变量类型    --->    返回值
Undefined           undefined
Boolean             boolean
Number              number
String              string
引用类型/Null        object
函数                 function
*/
// NaN表示非数字


// var a='10';
// var b=11;
// var c = a*b;
// console.log(c,typeof c)

// 数据类型的转换

// 转化为字符串
//     var num1=123;
//     var str1=num1.toString();
//     console.log(str1,typeof str1)

// 转化为数字
// 转化后的返回值只有十进制数和NaN两种
// parseInt()转化为整数     可有效提取整数
// parseFloat()转化为浮点数  有效提取小数
// Number()返回NaN
// var str11=true;
// var res13=parseInt(str11);
// console.log(res13);
// var res14=Number(str11);
// console.log(res14);

// 字符串转数字
//     纯数字字符串
//         var str1='123';
//         var num1=Number(str1);
//         console.log(num1,typeof num1);
//     含非数字内容字符串
//             var str1='abc123';
//             var num1=Number(str1);
//             console.log(num1,typeof num1);
// undefined转数字NaN
//     var unde = undefined;
//     var num1=Number(unde);
//     console.log(num1,typeof num1);

// null转数字0
//     var nu =null;
//     var num1=Number(nu);
//     console.log(num1,typeof num1);

// 布尔转数字true转成1 false转成0
//     var bool=true;
//     var num1=Number(bool);
//     console.log(num1,typeof num1);
//     var bool1=false;
//     var num2=Number(bool1);
//     console.log(num2,typeof num2);

// var a = '123asd';
// var b = 'asd123';
// var c = '1s2d3w22s';
// var d = '1334江苏!!';
// var f = '1.4985';
// var f1 = '1.5242';
// var aa = 'AF';
// var bb = '010';
// console.log(parseInt(a))
// console.log(parseInt(b))
// console.log(parseInt(c))
// console.log(parseInt(d))
// console.log(parseInt(f))
// console.log(parseInt(f1))
// console.log(parseInt(aa,16))
// console.log(parseInt(bb,8))

// 运算符

// 一元运算符
//     只有一个操作数的运算符
//     var num1= +'6';
//     console.log(num1,typeof num1);
//     var num2= +null;
//     console.log(num2,typeof num2);
//     var num3= +undefined;
//     console.log(num3,typeof num3);
//     var num4= +true;
//     console.log(num4,typeof num4);

// 赋值运算符
//     简单赋值运算符
//         格式:变量名=数据
//     复合赋值运算符
//         +=  -=  *=  /=  %=
//         a*=1+2 相当于a=a*(1+2)
//         var a=10
//         a*=100+30
//         console.log(a)
// 自增自减运算符
//     var a=10,b;
//     b=a++;
//     console.log(a);
//     console.log(b);
//     var num=10;
//     sun= num++ + 10;
//     console.log(sun);
//     console.log(num);


//     var a1=10,b;
//     b1=++a1;
//     console.log(a1);
//     console.log(b1);
//     var num1=10;
//     sun1=++num1+10;
//     console.log(sun1);
//     console.log(num1);

// 关系运算符
    // <  >  >=  <=  ==  ===  !=  !==
// 对于非数值类型进行比较时,会转化为数值再进行比较
// var a=1;
// console.log(a==true);
// console.log(a===true);
// console.log(a>'0');
// 对于两侧都是字符串的比较,不会转化为数字,而是按每一位字母的Unicode码的大小来进行运算
// console.log('a'<'b');
// console.log('abc'<'abd');
// console.log('你'<'我');
// console.log(null ==0);
// console.log(undefined ==0);
// console.log(NaN ==NaN);
// // 用 isNaN()函数来判断是否为NaN
// var num= NaN;
// console.log(isNaN(num));
// console.log(isNaN(1));
// console.log(isNaN(null));
// console.log(isNaN(undefined));
// console.log(isNaN('abc'));
// // undefined衍生与null
// console.log(null==undefined);
// console.log(null===undefined);

// ==进行比较会进行数据类型的转换,而===则不会

// 逻辑运算符
// &&  与运算
// ||  或运算
// !   非运算

// var a,b;
// b=(a=3,--a,a*5);
// console.log(b)

// 三元运算
// 条件表达式 ? 语句1 : 语句2;
/*
    接收用户输入的三个数,找出最大的数
    var num1,num2,num3,maxnum;
    num1=Number(prompt('请输入数字1'));
    num2=Number(prompt('请输入数字2'));
    num3=Number(prompt('请输入数字3'));
    maxnum=num1>num2?num1:num2;
    maxnum=maxnum>num3?maxnum:num3;
    console.log(maxnum);
*/


// if循环

/*
if(表达式1){
    代码块1;
}else if(表达式2){
    代码块2;
}......
else{
    代码块n;
}
*/

/*
switch (表达式){
    case 值1:代码1;
    break;
    case 值2:代码2;
    break;
    case 值3:代码3;
    break;
    case 值4:代码4;
    break;
    ......
    default:代码5;
    break;
}
*/
/*
var month = Number(prompt("月份"));
if(!isNaN(month)){
    switch (month){
        case 1:
        case 3:
        case 5:
        case 7:
        case 8:
        case 10:
        case 12:
            console.log("有31天")
            break;
        case 4:
        case 6:
        case 9:
        case 11:
            console.log("有30天")
            break;
        case 2:
            console.log("有28天")
            break;
    }
}else {
    console.log("输入有误")
}
*/


// while循环
// while (循环条件){
//     循环体;
//     计数器++;
// }

// for循环

// for (表达式1;表达式2;表达式3){
//     循环体;
// }

// 斐波那契数列
// var num1=1,num2=1,sun=0;
// for (var i=3;i<=12;i++){
//     sun=num1+num2;
//     num1=num2;
//     num2 =sun;
// }
// console.log(sun);

// for 循环遍历数组
// var array = new Array(1,2,'mjc',undefined,true,null,new Object);
// for(var i=0;i<array.length;i++){
//     console.log(array[i]);
// }

// 求数组里的最大值
// var array = new Array(1,6,9000,120,900,1000,0);
// for(var max = array[0],i=0;i<array.length;i++){
//     if(max<array[i]){
//         max=array[i];
//     }
// }
// console.log(max);

// 数组的倒序
// var array = new Array(1,2,3,4,6,5);
// for (var i=array.length-1;i>=0;i--){
//     console.log(array[i]);
// }

// Math函数
// Math.floor(x)
// 对数进行去尾取整
// Math.round(x)
// 对数进行四舍五入

// c=Math.round(4.5)
// alert(c)

// Date函数
// var date=new Date();
// console.log(date);
// 获取时间戳
// console.log(date.getTime());

// 获取年份
// console.log(date.getFullYear());

// 获取月份
// console.log(date.getMonth()+1);

// 获取日期
// console.log(date.getDate());

// 获取时
// console.log(date.getHours());

// 获取分
// console.log(date.getMinutes());

// 获取秒
// console.log(date.getSeconds());

// 获取星期
// console.log(date.getDay());

// 英文的日期的字符串
// console.log(date.toDateString());

// 数字的日期的字符串
// console.log(date.toLocaleDateString());

// 英文的时间的字符串
// console.log(date.toTimeString());

// 英文的时间的字符串
// console.log(date.toLocaleDateString());
