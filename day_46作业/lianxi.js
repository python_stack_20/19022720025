// // 正则匹配
    // var reg1=new RegExp('^[a-zA-Z][a-zA-Z0-9_]{5,11}')
    // var s1='bc1223';
    // reg1.test(s1);
    // console.log(reg1.test(s1));
    // console.log(reg1.test());   //test里面不写默认是undefined

    // var s2='hello world';
    // s2.match(/o/g);
    // console.log(s2.match(/o/g));//匹配s2里所有的字母'o',g代表全局即匹配所有;返回数组
    // console.log(s2.search(/h/));//查找并返回字母'h'对应的索引
    // console.log(s2.split(/o/));//将字符串按指定字符进行切割
    // console.log(s2.replace(/o/g,'s'));//将字符串中的所有o字母转化为字母s并返回
    //
    // var s1='name:Alex age:18';
    // console.log(s1.replace(/a/,'哈哈哈'));//将匹配到的第一个字母a转化为字符'哈哈哈'
    // console.log(s1.replace(/a/g,'哈哈哈'));//将匹配到的所有字母a转化为字符'哈哈哈'
    // console.log(s1.replace(/a/gi,'哈哈哈'));//将匹配到的第一个字母a或A转化为字符'哈哈哈'

    // var reg3=/^foo$/g;
    // //全局匹配时,test()函数不是从开头开始查找,而是从其上一次的最后的索引位置接着查找
    // //这里就是reg3.lastindex
    // console.log(reg3.test('foo'));//返回true
    // console.log(reg3.lastIndex);//3
    // console.log(reg3.test('foo'));//返回false
    // console.log(reg3.lastIndex);//0
    // console.log(reg3.test('foo'));//返回true
    // console.log(reg3.lastIndex);//3
    // console.log(reg3.test('foo'));//返回false


// boom

//windows对象
    // console.log(window.innerHeight);//浏览器窗口内部高度
    // console.log(window.innerWidth);//浏览器窗口内部宽度
    // console.log(window.open());//打开新窗口
    // console.log(window.close());//关闭js打开的新窗口





// // navigator对象
    // console.log(navigator.appName);//web浏览器全称
    // console.log(navigator.appVersion);//web浏览器厂商版本
    // console.log(navigator.userAgent);//客户端绝大部分信息
    // console.log(navigator.platform);//浏览器所在的操作系统

// //screen对象
    // console.log(screen.availWidth);查看可用的屏幕宽度
    // console.log(screen.availHeight);查看可用的屏幕高度

//history对象
    // history.forward()//前进一页
    // history.back()//后退一页


// location
    // console.log(location.href);//获取URL
    // location.href="https://www.baidu.com/"//跳转到百度
    // console.log(location.reload());//刷新页面

//弹出框
    //警告框
    // alert('你好啊');
    //确认框
    // confirm('are you sure?')
    // 提示框
    // prompt('请在下方输入','你的答案')

// 计时相关
    // var t = setTimeout('alert("5秒时间到了,你考虑好了吗?")',5000);//延时执行(一次性)
    // clearTimeout(t);//取消上面的延时执行
    var t1=setInterval('alert("哈哈,我又出现啦")',2000);//每间隔一段时间就执行
    // clearInterval(t1);//取消上面的定时执行


//DOM
//直接查找
    // console.log(document.getElementById('d1'));//根据id获取一个标签
    // console.log(document.getElementsByClassName('c1'));//根据类名获取一个数组
    // console.log(document.getElementsByTagName('p'));//根据标签名获取一个标签合集
//间接查找
    // console.log(document.getElementById('p1').parentElement);//获取父标签节点元素
    // console.log(document.getElementById('d1').children);//获取所有子标签
    // console.log(document.getElementById('d1').firstElementChild);//获取第一个子标签元素
    // console.log(document.getElementById('d1').lastElementChild);//获取最后一个子标签元素
    // console.log(document.getElementById('p1').nextElementSibling);//获取下一个兄弟标签元素
    // console.log(document.getElementById('p2').previousElementSibling);//获取上一个兄弟标签元素





// 节点操作
// 创建节点(标签)
// var divEle=document.createElement('div');
// console.log(divEle)
// 添加节点
// var imgEle=document.createElement('img');
// console.log(imgEle.setAttribute('src','11.jpg'));
// var d1Ele=document.getElementById('d1');
// console.log(d1Ele.appendChild(imgEle));
// 删除节点
// console.log(somenode.removeChild(要删除的节点))
// 替换节点
// console.log(somenode.replaceChild(newnode,某个节点))
// 属性节点
// var divEle= document.getElementById('d1');
// divEle.innerText;//获取该标签及其内部所有的文本内容
// divEle.innerHTML;//获取该标签内的所有内容,包括文本和标签
// 设置文本节点的值:
// var divELe = document.getElementById('d1');
// divELe.innerText='1';
// divELe.innerHTML='<p>2</p>';
// attribute操作
// var divEle = document.getElementById('d1');
// divEle.setAttribute('age','18');
// divEle.getAttribute('age');
// divEle.removeAttribute('age');


// 获取值操作
// var iEle=document.getElementById('i1');//input
// console.log(iEle.value);
// var sEle=document.getElementById('s1');//select
// console.log(sEle.value);
// var tEle=document.getElementById('t1');//textarea
// console.log(tEle.value);

//class操作
// className获取所有样式类名
// 首先获取标签对象
// 标签对象.classlist.remove(cls);//删除指定类
// classList.add(cls);//添加类
// classList.contains(cls);//存在返回true,否则返回false
// classList.toggle(cls);//切换



// 事件
// function changeColor(ths){
//     ths.style.backgroundColor="green";
// }

// var divEle2=document.getElementById('d2');
// divEle2.onclick=function () {
//     this.innerText="呵呵";
//
// }