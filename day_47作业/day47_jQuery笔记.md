## jQuery

#### jQuery介绍

- jquery是一个轻量级的兼容多浏览器的JS库
- jquery可以更方便地处理HTML Document、Events、实现动画效果、方便进行Ajax交互，极大简化JS编程

#### jquery的优势

1. 轻量级的js框架，不会影响页面的加载速度

2. DOM选择器丰富，可以用更少的代码筛选出所需要的对象

3. 链式表达式，即可把多个操作写成一行代码

4. 支持事件、样式、动画

5. 支持Ajax操作

6. 基本兼容主流浏览器

7. 有丰富的扩展插件

   

#### jquery内容

1. 选择器
2. 筛选器
3. 样式操作
4. 文本操作
5. 属性操作
6. 文档处理
7. 事件
8. 动画效果
9. 插件
10. each、data、Ajax

#### jquery版本

1.x：兼容IE678，使用最为广泛

2.x：不兼容IE678

3.x：不兼容IE678，只支持最新的浏览器

#### jquery对象

jquery对象是通过jquery包装DOM对象后产生的对象，可以使用jquery提供的方法来操作。例如:

​		$("#i1").html()，意思是获取id为i1的元素的html的代码，其中的html()就是jquery中的方法。就相当于document.getElementById("i1").innerHTML;

#### jquery的引入方式

jquery的引入方式有两种:

1. 直接从本地文件导入
2. 使用网络的资源,类似img标签里的src用法

#### jquery基础语法

$(selector).action()

其中,$(selector)表示找到某个标签，.action()过这个标签对象调用他的一些方法。

#### 查找标签

##### 基本选择器(同css)

###### id选择器

$("#id")

$("")是固定的格式,引号里写选择器，同css

###### 标签选择器

$("tagName")

###### class选择器

$(".className")

###### 配合使用

$("div .cl")

找到具有c1类的div标签

###### 所有元素选择器

$("*")

###### 组合选择器

$("#id,.className,tagName")

##### 层级选择器(同css)

$("x   y");		//查找x标签的所有后代y标签

$("x > y");		//查找x标签的所有的儿子标签y

$("x + y");		//找出x标签紧挨着的下一个y标签

$("x ~ y");		//找出标签x后的所有的兄弟标签y

##### 基本筛选器(选择之后进行过滤):

$(" div :first")							选取第一个div标签

$(" div :last")							选取最后一个div标签

$(" div :eq(index)")				选取所有索引值为指定值的div标签

$(" div :even")						选取所有索引值为偶数的div标签

$(" div :odd")					 	选取所有索引值为奇数的div标签

$(" div :gt(index)")				选取所有索引值大于给定值的div标签

$(" div :lt(index)")				选取所有索引值小于给定值的div标签

$(" div :not(元素选择器)")	移除所有满足not条件的div标签

$(" div :has(元素选择器)")	选取所有的后代满足条件的标签的div标签

##### 属性选择器

[属性]或[属性='属性值']或[属性!='属性值']

因为input标签有很多类型，所以多用于input标签

例如:

```javascript
<input type='text'>
<input type='password'>
<input type='checkbox'>
$("input[type='checkbox']");//选取checkbox类型的input标签
$("input[type!='text']");//选取类型不是text的input标签
```

##### 表单筛选器

```js
:text
:password
:file
:radio
:checkbox
:submit
:reset
:button

```

//例如
		$(":checkbox");//找到所有的checkbox类型的input标签
//表单对象属性

```
:enabled
:disabled
:checked
:selected
```

注意:

​		在我们使用checked属性去筛选时,会默认将input标签和select标签里含有checked属性的所有标签都筛选出来，所以当我们只需要筛选出含checked属性的input标签时,应该这么写:

```js
$("input:checked");
```

#### 筛选器方法

###### 下一个元素

```javascript
$("#id").next();				//找到指定id对应标签的下一个标签
$("#id").nextAll();				//找到指定id对应标签的之后所有的标签
$("#id").nextUntil("#i2");		//找到指定id对应标签的后id为i2的标签为止
```

###### 上一个元素

```javascript
$("#id").prev();				//找到指定id对应标签的上一个标签
$("#id").prevAll();				//找到指定id对应标签的之前所有的标签
$("#id").prevUntil("#i2");		//找到指定id对应标签的前id为i2的标签为止
```

###### 父亲元素

```javascript
$("#id").parent();				// 查找当前元素的父亲元素
$("#id").parents();				// 查找当前元素的所有的父辈元素
$("#id").parentsUntil('body');	//查找当前元素的所有的父辈元素，直到遇到匹配的那个元素为止，这里直到body标签，不包含body标签，基本选择器都可以放到这里面使用
```

###### 儿子和兄弟元素

```javascript
$("#id").children();				// 查找当前元素的所有的儿子元素(下一级)
$("#id").sibling();					// 查找当前元素的所有的兄弟元素(同级)
```

###### 查找

搜索所有与指定表达式匹配的元素。这个函数是找出正在处理的元素的后代元素的好方法。

```javascript
$("div").find("p");					//搜索所有div标签下的p标签
//等价于$("div p");
```

###### 筛选

筛选出与指定表达式匹配的元素集合。这个方法用于缩小匹配的范围。用逗号分隔多个表达式。

```
$("div").filter(".c1");				//从结果集中过滤出有c1样式类的，从所有的div标签中过滤出有class='c1'属性的div
//等价于$("div.c1");
```

###### 其他方法

```javascript
.first()							//获取匹配的第一个元素
.last()								//获取匹配的最后一个元素
.not()								//从匹配集中剔除符合not筛选条件的标签
.has()								//从匹配集中保留符合has条件的标签,其余剔除
.eq()								//从匹配集中返回指定索引值对应标签
```

#### 操作标签

##### 样式操作

###### 样式类

##### 位置操作

##### 文本操作

##### 属性操作

###### prop和attr的区别

##### 文档处理

###### 添加到指定元素内部的后面

###### 添加到指定元素内部的前面

###### 添加到指定元素外部的后面

###### 添加到指定元素外部的前面

###### 移除和清空元素

###### 替换

###### 克隆

#### 事件

##### 常用事件

##### 事件绑定

##### 移除事件

##### 阻止后续事件执行

##### 阻止事件冒泡

##### 事件委托

##### 页面载入

##### 与window.onload的区别

#### 动画效果

#### 补充

##### each

##### jQuery.each(collection, callback(indexInArray, valueOfElement))：

##### .each(function(index, Element))：

##### 终止each循环

#### .data()

##### .data(key, value): 设置值

##### .data(key): 取值，没有的话返回undefined

##### .removeData(key):

#### 插件













