from django.db import models


# Create your models here.

class Book(models.Model):
    book_name = models.CharField(max_length=32)
    price = models.FloatField()
    data = models.DateField()
    publisher = models.CharField(max_length=32)


class User_info(models.Model):
    username = models.CharField(max_length=16)
    password = models.CharField(max_length=32)