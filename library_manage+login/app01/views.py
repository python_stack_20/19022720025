from django.shortcuts import render,HttpResponse,redirect
from app01 import models
from app01.models import *
import hashlib


# Create your views here.


def home(request):  # 读取数据库中的内容，渲染至home.html返回给客户端
    # book_obj = models.Book(book_name='颈椎康复指南', price=250, date='2017-9-8',publisher='人民邮电出版社')
    # book_obj = models.Book(book_name='python', price=123, data='2012-12-12',publisher='人民出版社')
    # book_obj.save()
    # models.Book.objects.filter(pk=2).delete()

    if request.method == 'GET':
        all_book_objs = Book.objects.all()
        print(all_book_objs)
        print(all_book_objs.values())

        return render(request, 'home.html',{'all_book_objs':all_book_objs})  # 如果请求是get请求就返回home.html


def add(request):
    if request.method == 'GET':  # 点击提交按钮

        return render(request, 'add.html')
    else:  # 点击保存按钮（提交）
        # 提交过后怎么做？post将值提交到这里，接收，添加到数据库，重定向到home.html
        print(request.POST)
        book_info_dict = request.POST.dict()  # 将接收到的数据转为字典
        del book_info_dict['csrfmiddlewaretoken']
        print(book_info_dict)
        create_obj = Book.objects.create(**book_info_dict)  # 打散


        return redirect('home')


def edit(request,n):
    if request.method == 'GET':
        old_obj = Book.objects.get(pk=n)
        return render(request,'edit.html',{'old_obj':old_obj})
    else:
        book_info_dict = request.POST.dict()
        del book_info_dict['csrfmiddlewaretoken']
        print(book_info_dict)
        models.Book.objects.filter(pk=n).update(**book_info_dict)

        return redirect('home')




def dele(request,n):
    Book.objects.filter(pk=n).delete()
    return redirect('home')



def search(request):  # 接收数据，模糊匹配，渲染返回html文件

    print(request)
    print(request.GET)
    # all_books = models.Book.objects.all().values('book_name')


    # return render(request,'search.html')
    return HttpResponse('搜索功能尚未完工')

def hash(username,password):
    md5 = hashlib.md5(username.encode('utf-8'))
    md5.update(password.encode('utf-8'))
    return md5.hexdigest()
def login(request):
    if request.method == 'GET':

        return render(request,'login.html')

    else:

        username = request.POST['user']
        password = request.POST['password']
        userinfo = User_info.objects.all()
        print(userinfo.values())
        print(userinfo.values('username')[0]['username'])

        ret = hash(username,password)

        if username == userinfo.values('username')[0]['username'] and ret == userinfo.values('password')[0]['password']:
            return redirect('home')
        else:
            return HttpResponse("用户名或密码错误！")





# md5 = hashlib.md5('盐'.encode('utf-8'))  # 选择加密方式  加盐
# md5.update('alex3714'.encode('utf-8')) # 将明文转成字节然后进行加密
# print(md5.hexdigest()) # 生成密文

