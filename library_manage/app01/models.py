from django.db import models


# Create your models here.

class Book(models.Model):
    id = models.AutoField(primary_key=True)
    book_name = models.CharField(max_length=32)
    price = models.FloatField(max_length=(5, 2))
    data = models.DateField()
    publisher = models.CharField(max_length=32)
